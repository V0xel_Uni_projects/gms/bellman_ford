#include <fstream>
#include <iostream>
#include <vector>
constexpr int INF = 9999; //numeric limits does not work???

int main()
{
	std::ifstream input("./tests/in");
	std::ofstream output("./tests/main_out");
	std::cin.rdbuf(input.rdbuf());
	std::cout.rdbuf(output.rdbuf());

	int 
		tests = 0,
		verts = 0;
	int graph[100][100] = {{}};
	int costs[100] = {};

	std::cin >> tests;
	//All tests loop
	for (int i = 0; i < tests; ++i)
	{
		//Init cost table
		costs[0] = 0;
		for (int i = 1; i < 100; ++i)
		{
			costs[i] = INF;
		}

		std::cin >> verts;

		//Making graph
		int temp;
		for (int i = 0; i < verts; ++i)
		{
			for (int j = 0; j < verts; ++j)
			{
				std::cin >> temp;
				if (temp != 0)
				{
					graph[i][j] = temp;
				}
				else
				{
					graph[i][j] = INF;
				}
			}
		}

		//Bellman-Ford
		for (int n = 1; n <= verts - 1; ++n)
		{
			for (int i = 0; i < verts; ++i)
			{
				for (int j = 0; j < verts; ++j)
				{
					if (graph[i][j] != INF && costs[i] != INF && costs[i] + graph[i][j] < costs[j])
					{
						costs[j] = graph[i][j] + costs[i];
					}
				}
			}
			//Printing the result of current iteration
			for (int i = 0; i < verts; ++i)
			{
				if (costs[i] >= INF)
				{
					std::cout << 0 << " ";
				}
				else
				{
					std::cout << costs[i] << " ";
				}
			}
			std::cout << "\n";
		}
		std::cout << "\n";
	}
	

#if 0
	//print graph
	for (int i = 0; i < verts; ++i)
	{
		for (int j = 0; j < verts; ++j)
		{
			if (graph[i][j] == INF)
			{
				std::cout << 0 << " ";
			}
			else
			{
				std::cout << graph[i][j] << " ";
			}
			if (j == verts - 1)
			{
				std::cout << "\n";
			}
	}
}
#endif
	return 0;
}